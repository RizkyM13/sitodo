<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home' , 'App\Http\Controllers\HomeController@index')->name('home');
Route::post('/home/update/{id}' , 'App\Http\Controllers\HomeController@update')->name('update');
Route::post('/home/tambah' , 'App\Http\Controllers\HomeController@tambah')->name('tambah');
Route::get('/home/hapus/{id}' , 'App\Http\Controllers\HomeController@hapus')->name('hapus');
Route::get('/home/edit/{id}' , 'App\Http\Controllers\HomeController@edit')->name('edit');



