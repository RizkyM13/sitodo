<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/','App\Http\Controllers\ApiListController@get'); //read
Route::post('list/post', 'App\Http\Controllers\ApiListController@post'); //create
Route::put('/list/update/{id}', 'App\Http\Controllers\ApiListController@put'); //update
Route::delete('/list/delete/{id}', 'App\Http\Controllers\ApiListController@delete'); //delete

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
