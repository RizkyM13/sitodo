<?php

namespace App\Http\Controllers;

use App\Models\Api_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiListController extends Controller
{

    public function get() //read
    {
    	$result = DB::table("list")->orderby('id', "desc")->get();
    	return response()->json($result);
    }

    public function post() //create
    {
    	$content = request('content');
    	DB::table('list')
    	->insert([
    		'created_at' => date ('Y-m-d H:i:s'),
    		'content' 	 => $content
    		]);
    	return response()->json(['status' => true, 'message' => 'Data Ditambahkan']);

    }

    public function put($id) //update
    {
    	$content = request('content');
    	DB::table('list')
    	->where('id', $id)
    	->update([
    		'updated_at' => date('Y-m-d H:i:s'),
    		'content'    => $content
    	]);
    	return response()->json(['status' => true, 'message' => 'Data Diupdate']);
    }

    public function delete($id) //hapus
    {
    	DB::table('list')
    	->where('id', $id)
    	->delete();
    	return response()->json(['status' => true, 'message' => 'Data Dihapus']);
    }
}
