<?php

namespace App\Http\Controllers;

use App\Models\Home_model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
	public function index()
	{
		$data = Home_model::all();
		return view('home', ['data' => $data]);
	}

	public function tambah(Request $request)
 	{
 		$data = new Home_model();
 		$data->content = $request->content;
 		$data->save();
 		return redirect(route('home'));
 	}

 	public function edit($id)
 	{
 		$data = Home_model::find($id);
 		$datas = Home_model::all();
 		return view ('home', compact('data', 'datas'));
 	}

	public function update(Request $request, $id)
	{
		$data = Home_model::find($id);
		$data->content = $request->content;
		$data->save();
		return redirect(route('home'));
		
	}

	public function hapus($id)
	{
		$data = Home_model::find($id);
		$data->delete();
		return redirect(route('home'));
	}

}