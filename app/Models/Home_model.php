<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Home_model extends Model 

{
	use HasFactory;

	protected $table = 'list';
	protected $primaryKey = 'id';
	protected $fillable = ['content', 'status', 'created_at', 'updated_at'];
}

?>