<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template • Todo</title>
		<link rel="stylesheet" href="{{ assets('css/base.css') }}">
		<link rel="stylesheet" href="{{ assets('css/index.css') }}">
		<!-- CSS overrides - remove if you don't need it -->
		<link rel="stylesheet" href="{{ assets('css/app.css') }} ">
	</head>
	<body>
		<section class="todoapp">
			<header class="header">
				<h1>Super2Do</h1>
				<input class="new-todo" placeholder="What needs to be done?" autofocus>
			</header>