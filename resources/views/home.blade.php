<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Template • Todo</title>
		<link rel="stylesheet" href="css/base.css">
		<link rel="stylesheet" href="css/index.css">
		<!-- CSS overrides - remove if you don't need it -->
		<link rel="stylesheet" href="css/app.css">
	</head>
	<body>
		<section class="todoapp">
			
			
			<header class="header">
				<h1>Super2Do</h1>
				<form action="{{route('tambah')}}" method="post" class="data">
				@csrf
				<input type="text" name="content" class="new-todo" value="" placeholder="What needs to be done?" autofocus>
				</form>
			</header>
			
			<!-- This section should be hidden by default and shown when there are todos -->
			<section class="main">
				<input id="toggle-all" class="toggle-all" type="checkbox">
				<label for="toggle-all">Mark all as complete</label>
				<ul class="todo-list">
					@foreach ($data as $c)
					<li>
						<div class="view">
							<input class="toggle" type="checkbox" checked="">
							<label>{{ $c->content}}<label>
							<a href="{{route('hapus',['id' => $c->id ]) }}" class="destroy"></a>
						</div>
						<input class="edit" value="">
					</li>
					@endforeach
					
				</ul>
			</section>
			<!-- This footer should be hidden by default and shown when there are todos -->
			<footer class="footer">
				<!-- This should be `0 items left` by default -->
				<span class="todo-count"><strong>0</strong> item left</span>
				<!-- Remove this if you don't implement routing -->
				<ul class="filters">
					<li>
						<a class="selected" href="/home">All</a>
					</li>
					<li>
						<a href="">Active</a>
					</li>
					
					<li>
						<a class="checked" href="/home">Completed</a>
					</li>
					

				</ul>
				<!-- Hidden if no completed items are left ↓ -->
				<button class="clear-completed">Clear completed</button>
			</footer>
		</section>
		<!-- Scripts here. Don't remove ↓ -->
		<script src="js/app.js"></script>
	</body>
</html>
